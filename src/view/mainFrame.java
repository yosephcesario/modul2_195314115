/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Action;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import model.Penduduk;


//PERHATIKAN SINTAKSNYA BAIK BAIK, JANGAN DICOPAS PLOK TAPI DIKETIK ULANG SAMBIL DIPAHAMI
/**
 *
 * @author ASUS
 */
//Kelas mainFrame digunakan untuk membuat desain JFrame, didalamnya terdapat menu bar File
//Edit, dan Help
public class mainFrame extends JFrame implements ActionListener{

    //untuk membuat variabel menuBar
    private JMenuBar menuBar;
    //untuk membuat variabel menu item di menu Edit
    private JMenuItem menuItemEdit_tambahMhs;
    private JMenuItem menuItemEdit_tambahMasyarakat;
    private JMenuItem menuItemEdit_tambahUKM;
    //untuk membuat variabel menu item di menu File 
    private JMenuItem menuItemFile_lihatData;
    private JMenuItem menuItemFile_exit;
    //untuk membuat menu di menuBar
    private JMenu menu_File;
    private JMenu menu_Edit;
    private JMenu menu_Help;
    
    public mainFrame() {
        initComponents();
    }

    private void initComponents() {
        menuBar = new JMenuBar();
        //membuat menu bar dengan nama File, Edit, Help
        menu_File = new JMenu("File");
        menu_Edit = new JMenu("Edit");
        menu_Help = new JMenu("Help");

        menuBar.add(menu_File);
        menuBar.add(menu_Edit);
        menuBar.add(menu_Help);
        this.setJMenuBar(menuBar);

        menuItemEdit_tambahMhs = new JMenuItem("Tambah Mahasiswa");
        menuItemEdit_tambahMasyarakat = new JMenuItem("Tambah Masyarakat");
        menuItemEdit_tambahUKM = new JMenuItem("Tambah UKM");
        menuItemFile_lihatData = new JMenuItem("Lihat Data");
        menuItemFile_exit= new JMenuItem("Exit");

        menu_Edit.add(menuItemEdit_tambahMhs);
        menu_Edit.add(menuItemEdit_tambahMasyarakat);
        menu_Edit.add(menuItemEdit_tambahUKM);
        
        
        menu_File.add(menuItemFile_lihatData);
        menu_File.add(menuItemFile_exit);
        menuBar.add(menu_File);
        menuBar.add(menu_Edit);
        menuBar.add(menu_Help);
        
        
        //untuk membuat program berhenti ketika mengklik tombol x di pojok kanan atas
        menuItemFile_exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        
        //untuk membuka form tambah mahasiswa, menggunakan action listener
        menuItemEdit_tambahMhs.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tambahMahasiswa_dialog addMhs = new tambahMahasiswa_dialog();
                addMhs.setSize(400, 300);
                addMhs.setVisible(true);
            }
        });
        
        //untuk membuka form tambah masyarakat, menggunakan action listener
        menuItemEdit_tambahMasyarakat.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tambahMasyarakat_dialog addMasyarakat = new tambahMasyarakat_dialog();
                addMasyarakat.setSize(400, 300);
                addMasyarakat.setVisible(true);
            }
        });
        
        //untuk membuka form tambah ukm, menggunakan action listener
        menuItemEdit_tambahUKM.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tambahUKM_dialog addUKM = new tambahUKM_dialog();
                addUKM.setSize(400, 460);
                addUKM.setVisible(true);
            }
        });

    }

    //merupakan kelas main
    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                mainFrame main = new mainFrame();
                main.setSize(400, 500);
                main.setVisible(true);
                main.setResizable(false);
                main.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        
    }
}
