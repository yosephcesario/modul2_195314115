/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;
import model.Mahasiswa;

/**
 *
 * @author ASUS
 */
public class tambahMahasiswa_dialog extends JDialog implements ActionListener{
    private JLabel label_title;
    private JButton button_ok;
    private JLabel label_nama;
    private JLabel label_nim;
    private JLabel label_ttl;
    private JTextField textField_nama;
    private JTextField textField_nim;
    private JTextField textField_ttl;
    
    public tambahMahasiswa_dialog(){
        init();
    }
    
    public void init(){
        this.setLayout(null);
        
        label_title = new JLabel("Form Tambah Mahasiswa");
        label_title.setBounds(120, 10, 200, 20);
        this.add(label_title);
        
        label_nama = new JLabel("Nama \t :");
        label_nama.setBounds(122, 60, 100, 50);
        this.add(label_nama);
        textField_nama = new JTextField();
        textField_nama.setBounds(180, 75, 180, 20);
        this.add(textField_nama);
        
        label_nim = new JLabel("NIM \t :");
        label_nim.setBounds(133, 100, 100, 50);
        this.add(label_nim);
        textField_nim = new JTextField();
        textField_nim.setBounds(180, 115, 180, 20);
        this.add(textField_nim);
        
        label_ttl = new JLabel("Tempat & Tanggal Lahir \t :");
        label_ttl.setBounds(20, 155, 200, 20);
        this.add(label_ttl);
        textField_ttl = new JTextField();
        textField_ttl.setBounds(180, 155, 180, 20);
        this.add(textField_ttl);
        
        button_ok = new JButton("OK");
        button_ok.setBounds(150, 200, 100, 30);
        this.add(button_ok);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        //event yg akan terjadi ketika mengklik tombol OK
        if (e.getSource() == button_ok) {
            Mahasiswa mhs = new Mahasiswa();
            mhs.setNama(textField_nama.getText());
            mhs.setNim(textField_nim.getText());
            mhs.setTempatTanggalLahir(textField_ttl.getText());
            
        }
    }
}
